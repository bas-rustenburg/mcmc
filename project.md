# Ideas for MCMC app functionality to be added.

## Themes
Application THEMES by swapping around css files. Multiple color schemes, light, dark.
* OS specific
* Mac Style. (Ask Patrick nicely to borrow his mac.) 
  * Photon looks nice enough
* Windows color scheme
  * Figure out how to get this information.
* Some artsy ones, to make the application stand out.
    
Should the default application look light, dark, colorful?

How easy should it be for users to add their own color scheme?  If it is too easy to change, non-advanced users may mess up the look, and won't appreciate full advantage of the design. Prevent them from making it ugly.


* No menu option to change it in that case.

* Css file, generated template, editable by user.
* Auto try and load them. Make sure they have expected fields before loading?
* If a theme is nice, make way for user to share it?
    * Twitter,
    * Github,
    
Special theme that allows hue rotation?

### Things to look up

* How does steam do this?


## Interface control

Top right corner icons for resizing and closing windows. Could use system bar, which is easy, but not necessarily pretty.
* Make custom buttons, as opposed to using system style.

Save window configurations, and loaded data so that user can resume on demand.

Scrollable tabs containing traces, histograms, kdes?

Perhaps open a new window for every parameter

Click parameter in the graph to spawn new window.

Progress bar

Main MCMC window with an overview.

* Current parameter estimates and uncertainty
* Percentage completion
* Example traces?
* Estimator for convergence.

* Selecting priors and likelihood models from dropdown menu?
* OR, drag them onto a graphical model screen.
* Draw directed graph of the model *would be useful as separate function.*
* Hover over node to reveal parameters? Or click to expand
    

    

Model diagram

## Simulation setup
Adding observations in a flexible way. (csv, excel, json, yaml)


* Plot histogram of trace, and kde. (See example KDE javascript on google. Should be easy to find)
    
    Two options here. Have it separate, or, rotate it, and line it up with the trace, for added coolness
    
* Joint-posterior density plots.
    * E.g. combine, 2, 3, (or more if doable) different model parameters, and look at their distributions.
    
    * Also plot them singly in a row of violin plots.
    
    * Plot the observations in a meaningful way along with the posterior estimates and sampled family of models.

 
##  Practical/code issues

* Data needs to be accessible during simulation, In order for an animation to be possible.

    * Write to file during trace,
    * OR, a global JS function with attributes for each of the interesting traces. These should be acessible by the plotting functions.
    
    Parameters should append themselves to this upon instantiation of the priors.
    
    * Should be able to spawn a new window/div object for every trace. If divs, be sure to prevent cluttering.
    Perhaps, new tabs, and preset spaces.
    





    
* Set up unit testing for underlying JS mcmc code.

## Homework exercises for Bas

* Learn variational bayes, and see if the code can be adjusted to do both

* Bayes py has interesting code structure. But what is it good for?

    
    
