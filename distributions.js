/**
 *  Probability distribution functions for JS
 *
 */
var math = require('math');

module.exports = {

    dlaplace: function (x, loc, scale, log) {
        /**
         * Calculate the height of the Laplace distribution at x
         *
         * Arguments:
         * `x`, the x-value of the point
         * `loc`, location of the peak of the distribution
         * `scale`, the diversity of the distribution.
         * `log`, boolean, if true return log value of density
         */

            // Default values
        loc = typeof loc !== 'undefined' ? loc : 0.0;
        scale = typeof scale !== 'undefined' ? scale : 1.0;
        log = typeof log !== 'undefined' ? log : false;

        var prefactor = 1 / 2.0 * scale;
        var exponent = -Math.abs(x - loc) / scale;

        if (log) {
            return Math.log(prefactor) + exponent;
        } else {
            return prefactor * Math.exp(exponent);
        }
    },

    dlognorm: function (x, loc, scale, log) {
        /**
         * Calculate the height of the lognormal distribution at x
         *
         * Arguments:
         * `x`, the x-value of point
         * `loc`, location of the peak of the distribution of ln(x)
         * `scale`, standard deviation of distribution of ln(x)
         * `log`, boolean, if true return log value of density
         */

            // Default values
        loc = typeof loc !== 'undefined' ? loc : 0.0;
        scale = typeof scale !== 'undefined' ? scale : 1.0;
        log = typeof log !== 'undefined' ? log : false;

        if (x <= 0) {
            throw "Out of bounds, only defined for x > 0";
        }
        if (scale <= 0) {
            throw "Out of bounds, only defined for scale > 0";
        }


        var prefactor = 1.0 / (scale * x * Math.sqrt(2 * Math.PI));
        var exponent = -0.5 * Math.pow((Math.log(x) - loc) / scale, 2);

        if (log) {
            return Math.log(prefactor) + exponent;
        } else {
            return prefactor * Math.exp(exponent);
        }
    },

    dnorm: function (x, loc, scale, log) {
        /**
         * Calculate height of normal distribution at x
         *
         * Arguments:
         * `x`, x-value of point
         * `loc`, location of the peak of the distribution.
         * `scale`, standard deviation of distribution
         * `log`, boolean, if true return log value of density
         */

            // Default values
        loc = typeof loc !== 'undefined' ? loc : 0.0;
        scale = typeof scale !== 'undefined' ? scale : 1.0;
        log = typeof log !== 'undefined' ? log : false;

        var prefactor = 1.0 / (scale * Math.sqrt(2 * Math.PI));
        var exponent = -0.5 * Math.pow((x - loc) / scale, 2);

        if (log) {
            return Math.log(prefactor) + exponent;
        } else {
            return prefactor * Math.exp(exponent);
        }

    },

    dcauchy: function (x, loc, scale, log) {
        /**
         * Calculate the height of the Cauchy distribution at x
         *
         * Arguments:
         * `x`, x-value of point
         * `loc`, location of the peak of the distribution.
         * `scale`, half-width at half-maximum.
         * `log`, boolean, if true return log value of density
         */

            // Default values
        loc = typeof loc !== 'undefined' ? loc : 0.0;
        scale = typeof scale !== 'undefined' ? scale : 1.0;
        log = typeof log !== 'undefined' ? log : false;

        var xg = (x - loc) / scale;
        var dens = 1. / (Math.PI * scale * (1 + xg * xg));

        if (log) {
            return Math.log(dens);

        } else {
            return dens;
        }
    },

    dunif: function (x, lower, upper, log) {
        /**
         * Calculate height of uniform distribution at x
         *
         * Arguments:
         * `x`, x-value of point
         * `lower`, lower bound of distribution
         * `upper`, upper bound of distribution
         * `log`, boolean, if true return log value of density
         *
         * Note: throws error for zero probability if log set to true
         */

            // Default values
        lower = typeof lower !== 'undefined' ? lower : 0.0;
        upper = typeof upper !== 'undefined' ? upper : 1.0;
        log = typeof log !== 'undefined' ? log : false;

        var dens;
        if (lower <= x && x <= upper) {
            dens = 1 / (upper - lower);
        } else {
            dens = 0;
            if (log) {
                throw "Zero probability";
            }
        }
        if (log) {
            return Math.log(dens);
        } else {
            return dens;
        }
    },

    dchisq: function (x, k, log) {
        /**
         * Calculate height of chi square distribution at x
         *
         * Arguments:
         * `x`, x-value of point
         * `k`, degrees of freedom of distribution
         * `log`, boolean, if true return log value of density
         *
         * Note: throws error for zero probability if log set to true
         */
        if (typeof k === 'undefined') {
            throw "Need to provide degrees of freedom!";
        }

        log = typeof log !== 'undefined' ? log : false;


        var dens;
        if (x > 0) {
            var numerator = math.pow(x, k / 2 - 1) * math.exp(-x / 2);
            var denominator = math.pow(2, k / 2) * math.gamma(k / 2);
            dens = numerator / denominator;
        } else {
            dens = 0;
            if (log) {
                throw "Zero probability";
            }
        }

        if (log) {
            return Math.log(dens);
        } else {
            return dens;
        }
    }
};
