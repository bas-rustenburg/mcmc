var iqr = require('compute-iqr');
var _ = require('underscore');
var d3 = require('d3');


function kernelDensityEstimator(kernel, x) {
    // http://bl.ocks.org/mbostock/4341954
    return function (sample) {
        return x.map(function (x) {
            return [x, d3.mean(sample, function (v) {
                return kernel(x - v);
            })];
        });
    };
}

function epanechnikovKernel(bandwidth) {
    // http://bl.ocks.org/mbostock/4341954
    return function (u) {
        return Math.abs(u /= bandwidth) <= 1 ? .75 * (1 - u * u) / bandwidth : 0;
    };
}

function triangularKernel(bandwidth) {
    return function (u) {
        return Math.abs(u /= bandwidth) <= 1 ? (1 - Math.abs(u)) / bandwidth : 0;
    };
}

function quarticKernel(bandwidth) {
    return function (u) {
        return Math.abs(u /= bandwidth) <= 1 ? 15.0/16.0 * (1 - u * u) * (1 - u * u) / bandwidth : 0;
    };
}

var biweightKernel = quarticKernel;

function triweightKernel(bandwidth) {
    return function (u) {
        return Math.abs(u /= bandwidth) <= 1 ? 35.0/32.0 * Math.pow((1 - u * u), 3)  / bandwidth : 0;
    };
}

function tricubeKernel(bandwidth) {
    return function (u) {
        return Math.abs(u /= bandwidth) <= 1 ? 70.0/81.0 * Math.pow((1 - Math.pow(Math.abs(u),3) ),3)  / bandwidth : 0;
    };
}

function cosineKernel(bandwidth) {
    return function (u) {
        return Math.abs(u /= bandwidth) <= 1 ?  Math.PI/4 * Math.cos(Math.PI/2 * u)  / bandwidth : 0;
    };
}

function topHatKernel(bandwidth) {
    return function (u) {
        return Math.abs(u /= bandwidth) <= 1 ? 0.5 / bandwidth : 0;
    };
}

var uniformKernel = topHatKernel;

function gaussianKernel(bandwidth) {
    return function (u) {
        return ( 1 / (Math.sqrt(2 * Math.PI)) * Math.exp(-0.5 * Math.pow(u / bandwidth, 2))) / bandwidth;
    };

}

function logisticKernel(bandwidth) {
    return function (u) {
        return  (1.0/ (Math.exp(u/bandwidth) + 2 + Math.exp(-u/bandwidth) )) / bandwidth;
    };

}

function silvermanKernel(bandwidth) {
    return function (u) {
        var u_sqabs = Math.abs(u/bandwidth) / Math.sqrt(2);
        return (0.5 * Math.exp(-1 * u_sqabs) * Math.sin(u_sqabs + (Math.PI /4) ) ) / bandwidth;
    };

}

function laPlaceKernel(bandwidth) {
    return function (u) {
        return ( 1 / (Math.sqrt(2 * Math.PI)) * Math.exp(-0.5 * Math.pow(u / bandwidth, 2))) / bandwidth;
    };

}

function getNBins(trace, maxbins) {
    maxbins = typeof maxbins !== 'undefined' ? maxbins : 50;


    var h = 2. * iqr(trace) / (Math.pow(trace.length, 1. / 3.));
    // fall back to sqrt(a) bins if iqr is 0
    if (h == 0) {
        return Math.min(maxbins, Math.sqrt(trace.length));
    } else {
        return Math.min(maxbins, Math.ceil((_.max(trace) - _.min(trace)) / h));
    }
}

function scottsFactor1D(trace, ndims) {
    /*
     Determine optimal bandwidth for plotting 1-D array using Scotts factor.

     */
    ndims = typeof ndims !== 'undefined' ? ndims : 1;
    return Math.pow(trace.length, (-1. / (ndims + 4)));
}

function normBydX(bin) {
    bin.y /= bin.dx;
    return bin;
}

function plotDist(div, trace, x_label, margins, height, width) {
    /* Distribution of a 1-D trace.

     */

    var x = d3.scale.linear()
        .domain([d3.min(trace), d3.max(trace)])
        .range([0, width]);

    var nBins = getNBins(trace);
    var data = d3.layout.histogram()
        .frequency(false)
        .bins(x.ticks(nBins))
    (trace);

    var kde = kernelDensityEstimator(uniformKernel(scottsFactor1D(trace)), x.ticks(150));

    // Normalize histogram by Area (instead of sum)
    data = _.map(data, normBydX);

    var y = d3.scale.linear()
        .domain([0, d3.max(data, function (d) {
            return d.y;
        })])
        .range([height, 0]);

    var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom");

    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left");

    var line = d3.svg.line()
        .x(function (d) {
            return x(d[0]);
        })
        .y(function (d) {
            return y(d[1]);
        });

    var area = d3.svg.area()
        .x(function (d) {
            return x(d[0]);
        })
        .y0(height)
        .y1(function (d) {
            return y(d[1]);
        });



    //.bins(x.ticks(Math.floor(Math.pow(trace.length, 1./3.))));
    var graph = d3.select(div).append("svg")
        .attr("width", width + margins.left + margins.right)
        .attr("height", height + margins.top + margins.bottom)
        .append("g")
        .attr("transform", "translate(" + margins.left + "," + margins.top + ")");


    graph.append("path")
        .datum(kde(trace))
        .attr("class", "area")
        .attr("d", area);

    graph.append("path")
        .datum(kde(trace))
        .attr("class", "kde")
        .attr("d", line);


    graph.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)
        .append("text")
        .attr("class", "label")
        .attr("x", width)
        .attr("y", height * -0.96)
        .style("text-anchor", "end")
        .text(x_label);

    graph.append("g")
        .attr("class", "y axis")
        .call(yAxis);

    graph.selectAll(".bar")
        .data(data)
        .enter().insert("rect", ".axis")
        .attr("class", "bar")
        .attr("x", function (d) {
            return x(d.x) + 1;
        })
        .attr("y", function (d) {
            return y(d.y);
        })
        .attr("width", x(data[0].dx + data[0].x) - x(data[0].x) - 1)
        .attr("height", function (d) {
            return height - y(d.y);
        });


    return graph;

}


function plotTrace(div, trace, plot_title, margins, height, width) {
    /* 1-D graph tracing a variable

     */
    // create a simple data array that we'll plot with a line (this array represents only the Y values, X will just be the index location)

    // X scale will fit all values from data[] within pixels 0-w


    var x = d3.scale.linear().domain([0, trace.length]).range([0, width]);

    // Y scale will fit values from 0-10 within pixels h-0 (Note the inverted domain for the y-scale: bigger is up!)
    // var y = d3.scale.linear().domain([0, 10]).range([h, 0]);
    // automatically determining max range can work something like this
    var y = d3.scale.linear().domain([d3.min(trace), d3.max(trace)]).range([height, 0]);
    // create a line function that can convert data[] into x and y points
    var line = d3.svg.line()
        // assign the X function to plot our line as we wish
        .x(function (d, i) {
            // verbose logging to show what's actually being done
            //console.log('Plotting X value for data point: ' + d + ' using index: ' + i + ' to be at: ' + x(i) + ' using our xScale.');
            // return the X coordinate where we want to plot this datapoint
            return x(i);
        })
        .y(function (d) {
            // verbose logging to show what's actually being done
            //console.log('Plotting Y value for data point: ' + d + ' to be at: ' + y(d) + " using our yScale.");
            // return the Y coordinate where we want to plot this datapoint
            return y(d);
        });
    // Add an SVG element with the desired dimensions and margin.
    var graph = d3.select(div).append("svg:svg")
        .attr("width", width + margins.left + margins.right)
        .attr("height", height + margins.top + margins.bottom)
        .append("svg:g")
        .attr("transform", "translate(" + margins.right + "," + margins.top + ")");

    // create yAxis
    var xAxis = d3.svg.axis().scale(x).tickSize(-height).ticks(5).tickSubdivide(true);
    // Add the x-axis.
    graph.append("svg:g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);
    // create left yAxis
    var yAxisLeft = d3.svg.axis().scale(y).ticks(4).orient("left");
    // Add the y-axis to the left
    graph.append("svg:g")
        .attr("class", "y axis")
        .attr("transform", "translate(-25,0)")
        .call(yAxisLeft);

    // Add the line by appending an svg:path element with the trace line we created above
    // do this AFTER the axes above so that the line is above the tick-lines
    graph.append("svg:path").attr("d", line(trace)).attr("class", "line");

    // Add the title
    graph.append("text")
        .attr("x", (width) / 2)
        .attr("y", 0)
        .attr("text-anchor", "middle")
        .style("font-size", "16px")
        .text(plot_title);
    return graph;
}

