// A linear regression model
var LinearRegressionModel = function () {};

LinearRegressionModel.prototype.modelFunction = function (param_estimates, observations) {
        var predictions = new Array(observations.length);
        observations.forEach(function(d, i) {
            // You can target your columns using this fucntion.
            // function(d) {return d.colname}
            // Here I'm parsing your time and val col numbers to actual numbers after importing.

            predictions[i] = (param_estimates.a * d.x + param_estimates.b);
        })

        return predictions;
    };

LinearRegressionModel.prototype.priors = {
        // Prior distributions for each parameter
        a: function (x) {
            return dist.dunif(x, -20, 20, true)
        },
        b: function (x) {
            return dist.dunif(x, -30, 30, true)
        },
        sigma: function (x) {
            return dist.dunif(Math.log(x), 0, 4, true)
        }
    };

LinearRegressionModel.prototype.likelihood = function (observation, prediction, sigma) {
        // Likelihood distribution for a single observation given a single prediction.
        return dist.dnorm(observation, prediction, sigma, true);
    };

LinearRegressionModel.prototype.logPrior = function (param_estimates) {
        /**
         * Sums over the prior probabilities.
         *
         * Arguments:
         * `param_estimates`, object containing current parameter estimates.
         *
         * Notes:
         * Will throw an error if a parameter value has no prior defined.
         */
        var total = 0;
        var priors = this.priors;
        for (var param in param_estimates) {
            if (param_estimates.hasOwnProperty(param)) {
                if (this.priors.hasOwnProperty(param)) {
                    total += priors[param](param_estimates[param]);
                }
            }
        }
        return total;
    };

LinearRegressionModel.prototype.logLikelihood = function (param_estimates, observations) {
        /**
         * Sums over the individual likelihoods of observations
         *
         * Arguments:
         * `param_estimates`, object containing current parameter estimates.
         * `observations`, observed data in format [[x1,y1], .. [xn,yn]]
         *
         */

        var predictions = this.modelFunction(param_estimates, observations);
        var singleLikelihoods = new Array(observations.length);
        var likelihood = this.likelihood;

        observations.forEach(function(d, i) {
            // You can target your columns using this fucntion.
            // function(d) {return d.colname}
            // Here I'm parsing your time and val col numbers to actual numbers after importing.
            var observation = d.y;
            var prediction = predictions[i];
            var sigma = param_estimates.sigma;
            singleLikelihoods[i] = likelihood(observation, prediction, sigma);
        })

        return singleLikelihoods.reduce(function (total, num) {
            return total + num;
        }, 0);
    };

LinearRegressionModel.prototype.logPosterior = function (param_estimates, observations) {
        // Simply sums up the calculated likelihood and prior density
        var logLikelihood = this.logLikelihood(param_estimates, observations);
        var logPrior = this.logPrior(param_estimates);
        var logposterior = logLikelihood + logPrior;
        if (logposterior === -Infinity) {
            throw "Zero probability posterior";
        } else {
            return {
                'logposterior': logposterior,
                'loglikelihood': logLikelihood,
                'logprior': logPrior
            };
        }
};

