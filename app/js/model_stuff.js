var generate_plots = function (traceData) {

    var plotColors = ["rgba(88, 50, 50, 1.0)", "rgba(38, 103, 177, 1.0)", "rgba(51, 153, 102, 1.0)", "rgba(64, 67, 65, 1.0)"];

    var m = {
        top: 20,
        bottom: 20,
        left: 80,
        right: 20
    };
    var wd = parseInt(d3.select("#dists1").style('width')) - m.left - m.right; // width of distribution
    var w = parseInt(d3.select("#traces1").style('width')) - m.left - m.right; // width of trace
    var h = 250 - m.top - m.bottom; // height
    var plotVars = ['a', 'b', 'sigma', 'logposterior'];
    var newdiv, trace, dist;

    for (var v = 0; v < plotVars.length; v++) {
        newdiv = plotVars[v] + "trace1";
        trace = jQuery('<div/>', {
            id: newdiv,
            class: "trace"
        }).appendTo('#traces1');
        plotTrace("#" + newdiv, traceData.chain[plotVars[v]], plotVars[v], m, h, w);
        trace.find('.line').css({
            "stroke": plotColors[v]
        });
    }
    for (var vv = 0; vv < plotVars.length; vv++) {
        newdiv = plotVars[vv] + "dist1";
        dist = jQuery('<div/>', {
            id: newdiv,
            class: "dist"
        }).appendTo('#dists1');
        plotDist("#" + newdiv, traceData.chain[plotVars[vv]], plotVars[vv], m, h, wd);
        dist.find('.area').css({
            "fill": plotColors[vv]
        });
        dist.find('.kde').css({
            "stroke": plotColors[vv]
        });
    }


};

var validate_model = function (model, initialConditions) {
    var priors = model.priors;

    try {
        for (var param in initialConditions) {
            if (initialConditions.hasOwnProperty(param)) {
                if (priors.hasOwnProperty(param)) {
                    var y = priors[param](initialConditions[param]);
                }
            }
        }
    } catch (err) {
        if (err == "Zero probability") {
            alert("Invalid initial setting for '" + param + "': " + err );
        } else {
            alert("Unknown error. Your developer messed something up: \n" + err);
        }
        return false;
    }

    return true;


};


var run_model = function (initialConditions, proposalSigma) {
    var observedValues = [
        {x: -6, y: -27.2216405516},
        {x: -5, y: -25.578474981},
        {x: -4, y: -28.2391251881},
        {x: -3, y: -15.324821224},
        {x: -2, y: -10.5410117265},
        {x: -1, y: -13.9645777125},
        {x: 0, y: -17.5409161763},
        {x: -1, y: -9.6421903174},
        {x: -2, y: -5.1665845153},
        {x: -3, y: -6.1042541435},
        {x: -4, y: -4.6858872701},
        {x: -5, y: -5.982853078},
        {x: -6, y: -12.3671583525}];


    var nIter = 100000;
    var burnIn = 1000;
    var nthin = 100;

    var model = new LinearRegressionModel();
    var tracker1 = MetropolisHastingsSampler(model, initialConditions, proposalSigma, observedValues, nIter).discardEquilibration(burnIn).thinSamples(nthin);
    //    var tracker1 = GMTMSampler(model, initialConditions, proposalSigma, observedValues, nTrials, nIter, 'MTM').discardEquilibration(burnIn).thinSamples(nthin);
    //    var tracker1 = DelayedRejectionSampler(model, initialConditions, proposalSigma, observedValues, nTrials, nIter).discardEquilibration(burnIn).thinSamples(nthin);

    return tracker1;
};
