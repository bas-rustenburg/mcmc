# Markov chain monte carlo in javascript

This repository contains an mcmc library implemented in javascript.
It is a work in progress.

Run app from this directory using the command

```shell
electron app
```

assuming electron is available on the command line.

The gui is powered by electron:

http://electron.atom.io/
