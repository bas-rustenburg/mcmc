import pymc
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

data = { 'weight':   [ -15,
     -14,
     -13,
     -12,
     -11,
     -10,
     -9,
     -8,
     -7,
     -6,
     -5,
     -4,
     -3,
     -2,
     -1,
     0,
     1,
     2,
     3,
     4,
     5,
     6,
     7,
     8,
     9,
     10,
     11,
     12,
     13,
     14,
     15 ],
  'mpg':   [ -67.19713671710626,
     -47.72316682453724,
     -68.93065997498904,
     -74.03710274119561,
     -67.63167721681026,
     -56.6198894949634,
     -40.46666385694161,
     -43.06051729915652,
     -22.713923786432083,
     -28.37099709595141,
     -22.90026899132633,
     -38.44225517385114,
     -13.653596944876963,
     8.859325551742991,
     -9.472441352045283,
     -2.1440709448241697,
     1.0228390751542245,
     2.343432741548588,
     6.143418211570502,
     12.770871068822707,
     26.744630821051068,
     43.73090362879272,
     53.92738202959771,
     37.000801628425734,
     65.42846560157986,
     54.57102436055708,
     70.87386489816069,
     46.72950914103257,
     68.01030377804764,
     80.4857309011872,
     68.58859399599187 ] }

float_df = pd.DataFrame.from_dict(data)

# NOTE: the linear regression model we're trying to solve for is
# given by:
# y = b0 + b1(x) + error
# where b0 is the intercept term, b1 is the slope, and error is
# the error

# model the intercept/slope terms of our model as
# normal random variables with comically large variances
b0 = pymc.Uniform("b", lower=-5, upper=5, value=0)
b1 = pymc.Uniform("a", lower=0, upper=10, value=4)

# model our error term as a uniform random variable
err = pymc.Uniform("sig", lower=0, upper=30, value=10)


# "model" the observed x values as a normal random variable
# in reality, because x is observed, it doesn't actually matter
# how we choose to model x -- PyMC isn't going to change x's values
x_weight = pymc.Normal("weight", 0, 1, value=np.array(float_df["weight"]), observed=True)

# this is the heart of our model: given our b0, b1 and our x observations, we want
# to predict y
@pymc.deterministic
def pred(b0=b0, b1=b1, x=x_weight):
    return b0 + b1*x

# "model" the observed y values: again, I reiterate that PyMC treats y as
# evidence -- as fixed; it's going to use this as evidence in updating our belief
# about the "unobserved" parameters (b0, b1, and err), which are the
# things we're interested in inferring after all
y = pymc.Normal("y", pred, err, value=np.array(float_df["mpg"]), observed=True)

# put everything we've modeled into a PyMC model
model = pymc.Model([pred, b0, b1, y, err, x_weight])

# prepare for MCMC
mcmc = pymc.MCMC(model)

# sample from our posterior distribution 50,000 times, but
# throw the first 20,000 samples out to ensure that we're only
# sampling from our steady-state posterior distribution
mcmc.sample(1000000)
plt.hist(mcmc.trace('a')[:], bins=50)
plt.show()
