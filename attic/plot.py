import csv
import seaborn as sns
from matplotlib import pyplot as plt

with open("chain.txt", 'r') as datafile:
    csvdata = csv.reader(datafile, delimiter=',')
    a = list()
    b = list()
    s = list()
    for row in csvdata:
        a.append(float(row[0]))
        b.append(float(row[1]))
        s.append(float(row[2]))

sns.distplot(a, label='A')
sns.distplot(b, label='B')
sns.distplot(s, label='sigma' )
plt.legend()
plt.axvline(sum(a)/len(a))
plt.axvline(5,color='r')
plt.axvline(sum(b)/len(b))
plt.axvline(0, color='r')
plt.axvline(sum(s)/len(s))
plt.axvline(10, color='r'   )
plt.show()
