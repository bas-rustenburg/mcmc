
import numpy as np
from scipy.stats import norm, uniform
true_mean_A = 5
true_mean_B = 0
true_sigma = 10
sampleSize = 31

xValues = np.array(sampleSize)
yValues = np.array(sampleSize)

def likelihood(a,b, sigma, obs, pred=np.ndarray(sampleSize), singleLikelihoods=np.ndarray(sampleSize)):
    
    i=0
    while i < pred.size:
        pred[i] = a * obs['x'][i] +b
        i +=1
    
    i=0
    while i < singleLikelihoods.size:
        dist = norm(pred[i], sigma)
        singleLikelihoods[i] = dist.logpdf(obs['y'][i])
        i +=1
    
    print(singleLikelihoods)
    return np.sum(singleLikelihoods)

def prior(a,b,sigma):
    aPrior= uniform(loc=0, scale=10).logpdf(a)
    bPrior= uniform(loc=-5, scale=10).logpdf(b)
    sigmaPrior = uniform(loc=0,scale=30).logpdf(np.log(sigma))
    return (aPrior + bPrior + sigmaPrior)
    
def posterior(a, b, sigma, obs):
    return (likelihood(a, b, sigma, obs) + prior(a, b, sigma))
    
def propose(mus, sigmas):
    proposal = list()
    dists = list()
    for m,s in zip(mus,sigmas):
        dists.append(norm(m,s))
        
    for d in dists:
        proposal.append(norm.rvs(1))
        
    return proposal

def sampleMCMC(initialConditions, obs, nIter):
    unif = uniform()
    chain = np.empty([nIter,3])
    chain[0] = initialConditions
    chaincopy = np.append(chain[0], obs)
    chainpost = posterior(*chaincopy)
    i=0
    try:
        while i< nIter:
            proposal = propose(chain[i], [0.1, 0.5, 0.3])
            propcopy = np.append(proposal, obs)
            proppost = posterior(*propcopy)
            acceptanceProbability= np.exp(proppost-chainpost)
            
            if unif.rvs(1) < acceptanceProbability:
                chain[i+1] = proposal
                chainpost = proppost
            else:
                chain[i+1] = chain[i]
                
            i +=1
    except IndexError:
        pass
    return chain

xValues = np.linspace(-(sampleSize - 1) / 2, (sampleSize - 1) / 2, sampleSize)

def axbc(x):
    ydist = norm(loc=0, scale=true_sigma)
    return true_mean_A * x + true_mean_B + ydist.rvs(1)

yValues = list(map(axbc,xValues))

observedValues = dict(x=xValues, y=yValues)

print(observedValues)

initialConditions = [4, 0, 10]
nIter = 1000
# chain = sampleMCMC(initialConditions, observedValues, nIter)

np.savetxt("chain.txt", chain, delimiter=",")
     
