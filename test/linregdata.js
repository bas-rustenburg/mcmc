var fs = require('fs');
var true_mean_A = 3;
var true_mean_B = -12;
var true_sigma = 5;
var sampleSize = 13;
var xValues = [];
var yValues = [];

// Get some x values
for (var i = -(sampleSize - 1) / 2; i <= (sampleSize - 1) / 2; i++) {
    xValues.push(i);
}

// Randomly generate input data
for (var i = 0; i < xValues.length; i++) {
    yValues.push(true_mean_A * xValues[i] + true_mean_B + randgen.rnorm(0, true_sigma));
}

var observedValues = {};
observedValues['x'] = xValues;
observedValues['y'] = yValues;

fs.writeFileSync('./data.json', JSON.stringify(observedValues) , 'utf-8');
