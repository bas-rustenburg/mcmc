// Javascript MCMC
var randgen = require('randgen');
var dist = require('../distributions');


function add_data(object1, object2) {
    // Extend/override object1 with new data from object2
    var newobject = {};
    for (var key1 in object1) {
        newobject[key1] = object1[key1];
    }
    for (var key2 in object2) {
        newobject[key2] = object2[key2];
    }
    return newobject;
}

var Tracker = function (variables, nIterations) {
    /**
     * Tracker object to keep track of all samples in a MCMC chain.
     *
     * Arguments:
     * `variables`, array of strings. Will keep track of these variables.
     * `nIterations`, integer, optional. Used to preallocate sample array length.
     *
     * Note:
     * If `nIterations` is specified, an index is always needed when setting an iteration.
     */
    nIterations = typeof nIterations !== 'undefined' ? nIterations : 0;

    this.chain = {};

    // Add an array to keep track of the specified variables of interest.
    for (var v = 0; v < variables.length; v++) {
        this.chain[variables[v]] = new Array(nIterations);
    }

    this.discardEquilibration = function (nEquil) {
        /**
         * Discard first nEquil samples as equilibration
         *
         * Parameters:
         * `nEquil`, integer. Number of samples to discard from start of chain.
         */
        for (var key in this.chain) {
            if (this.chain.hasOwnProperty(key)) {
                this.chain[key] = this.chain[key].slice(nEquil);
            }
        }
        return this;
    };

        this.thinSamples = function (nThin) {
        /**
         * Filter the chain, keep only every nThin-th sample
         *
         * Parameters:
         * `nThin`, integer. N-th sample to keep.
         */

        // Define the filter
        var thinArray = function (value, index) {
            // value is ignored.
            return index % nThin === 0;
        };

        // Keep every nThin-th sample.
        for (var key in this.chain) {
            if (this.chain.hasOwnProperty(key)) {
                this.chain[key] = this.chain[key].filter(thinArray);
            }
        }
        return this;
    };

    this.getIteration = function (index) {
        /**
         * Access an iteration by index.
         */
        var iteration = {};
        for (var key in this.chain) {
            if (this.chain.hasOwnProperty(key)) {
                iteration[key] = this.chain[key][index];
            }
        }
        return iteration;
    };

    this.setIteration = function (step, index, debug) {
        /**
         * Adds a new iteration to the chain
         *
         * Parameters:
         * `step`, object containing value for each tracked variable, with variable names as keys.
         * `index`, integer, optional. Needs to be specified if array size was preallocated.
         * `debug`, boolean, optional. If true, print out a message if a property could not be stored.
         *
         * Note:
         * If `index` not supplied, this function pushes step to the end of the chain.
         */

        debug = typeof debug !== 'undefined' ? debug : false;

        for (var key in step) {
            if (this.chain.hasOwnProperty(key)) {

                // Add to specified location
                if (typeof(index) !== 'undefined') {
                    this.chain[key][index] = step[key];
                    // If no index is provided, append to end
                } else if (nIterations === 0) {
                    this.chain[key].push(step[key]);
                    // Don't know where to add value.
                } else {
                    throw "Index needs to be provided if model chain length is preallocated.";
                }

            } else if (debug) {
                console.log("Not storing property in chain: " + key);
            }
        }
    };
};

var propose = function (model, mus, sigmas) {
    /**
     * Make a proposal for all parameters using proposal distribution T(u,·),
     * where T is a combination of normal distributions with mu as the mean, and standard deviation sigma.
     *
     * Arguments:
     * `mus`, object containing current value for each parameter, with parameter names as keys.
     * `sigmas`, corresponding sigma for each parameter, with parameter names as keys.
     *
     */

    var proposal = {};
    for (var param in mus) {
        if (model.priors.hasOwnProperty(param)) {
            proposal[param] = randgen.rnorm(mus[param], sigmas[param]);
        }
    }
    return proposal;
};

var proposalDensity = function (model, a, b, sigmas, log) {
    /**
     * Return density of a proposal on the proposal distribution T(a,b),
     * where T is a combination of normal distributions with a as the mean, and standard deviation sigma.
     *
     * Arguments:
     * `a`, object containing current value for each parameter, with parameter names as keys.
     * `b`, object containing proposed value for each parameter, with parameter names as keys.
     * `sigmas`, object containing corresponding sigma for each parameter, with parameter names as keys.
     * `log`, boolean, return probability as a log value. Default=true.
     * Returns:
     *
     */
    log = typeof log !== 'undefined' ? log : true;
    var probability = 0;
    for (var param in a) {
        if (model.priors.hasOwnProperty(param)) {
            probability += dist.dnorm(a[param], b[param], sigmas[param], log)
        }
    }
    return probability;
};

var wStarMTMI = function (model, u, v, sigmas) {
    /** w* function of MTM-I scheme w^*(u,v) = π(u)T(u,v)
     *
     * Parameters:
     * `model`, model object
     * `u`, object containing proposed value for each parameter
     *   and corresponding posterior density pi(u).
     * `v`, object containing current value for each parameter, with parameter names as keys
     * `sigmas`, object containing corresponding proposal sigma (as in T(u,v)) for each parameter, with parameter names as keys.
     *
     * Returns:
     * `weight`: the unnormalized weight of a sample, calculated as  π(u)T(u,v).
     */


    var weight = u.logposterior; // π(u)

    for (var param in u) {
        if (model.priors.hasOwnProperty(param)) {

            weight += dist.dnorm(u[param], v[param], sigmas[param], true);

        }
    }
    return Math.exp(weight);
};

var wStarMTMinv = function (model, u, v, sigmas) {
    /** w* function of MTM-inv scheme w^*(u,v) = π(u)T(u,v)
     *
     * Parameters:
     * `u`, object containing proposed value for each parameter, with parameter names as keys,
     *   and corresponding posterior density pi(u).
     * `v`, object containing current value for each parameter, with parameter names as keys
     * `sigmas`, object containing corresponding proposal sigma (as in T(u,v)) for each parameter, with parameter names as keys.
     *
     * Returns:
     * `weight`: the unnormalized weight of a sample, calculated as  π(u)/T(v,u).
     */


    var weight = u.logposterior; // π(u)

    for (var param in u) {
        if (model.priors.hasOwnProperty(param)) {
            weight -= dist.dnorm(v[param], u[param], sigmas[param], true);
        }
    }
    return Math.exp(weight);
};

var wStarMTM = function (model, u) {
    /** w* function of MTM-I scheme w^*(u,v) = π(u)T(u,v)
     *
     * Parameters:
     * `u`, object containing proposed value for each parameter, with parameter names as keys,
     *   and corresponding posterior density pi(u).
     *
     * Returns:
     * `weight`: the unnormalized weight of a sample, calculated as  π(u).
     */
    var weight = u.logposterior; // π(u)


    return Math.exp(weight);
};

var drawWeightedSample = function (weights) {
    /** Given an array of weights, draw a random sample and return the index
     * Divides a space between 0 and the total of all weights up in bins.
     * Bin size determined by weight size, done cumulatively. No need to sort or normalize.
     *
     * Arguments:
     * `weights`, 1-D array of weights.
     *
     * Returns:
     * `current` index of the bin in which the random sample falls

     */
    var total = weights.reduce(function (total, num) {
        return total + num
    });
    var draw = randgen.runif(Math.min(0, total), Math.max(0, total));
    var binEdge = 0.0;

    for (var current = 0; current < weights.length; current++) {
        var weight = weights[current];
        binEdge += weight;
        if (draw <= binEdge) {
            return [current, weight / total];
        }
    }

};

var MetropolisHastingsSampler = function(model, firstGuess, proposalSigma, observations, nIter) {
    /**
     * Sample parameters from the posterior distribution using Metropolis-Hastings.
     *
     * Arguments:
     * `model`, object containing the posterior, likelihood and prior functions.
     * `firstGuess`, object containing initial guess for each parameter, with parameter names as keys.
     * `proposalSigma`, object containing corresponding proposal standard deviation for each parameter, with parameter names as keys.
     * `observations`, object containing individual observation values
     * `nIter`, integer number of iterations to run sampler.
     *
     * Returns:
     * `tracker`, updated tracker object containing samples
     */
     
    // Set initial entry to be first guess and calculate the initial posterior density
    firstGuess = add_data(firstGuess, model.logPosterior(firstGuess, observations));
    var tracker = new Tracker(Object.keys(firstGuess), nIter);
    tracker.setIteration(firstGuess, 0);

    var currentState;
    var proposal, forwardProb, backwardProb;
    var acceptanceProbability;
    var accepted = 0;
    var rejected = 0;
    var n_zeroes = 0;

    // main sampling loop
    for (var i = 0; i <= nIter; i++) {
        // Make a new proposal
        currentState = tracker.getIteration(i);
        proposal = propose(model, currentState, proposalSigma);

        // If proposal lies outside of target support.
        try {
            proposal = add_data(proposal, model.logPosterior(proposal, observations));
        } catch (err) {
            if (err == "Zero probability") {
                tracker.setIteration(currentState, i + 1);
                rejected++;
                n_zeroes++;
                continue;
            }
        }

        forwardProb = proposalDensity(model, proposal, currentState, proposalSigma, true);
        backwardProb = proposalDensity(model, currentState, proposal, proposalSigma, true);

        var numerator = Math.exp(proposal.logposterior + backwardProb);
        var denominator = Math.exp(currentState.logposterior + forwardProb);

        acceptanceProbability = numerator / denominator;

        //  If α ≥ 1, then proposal is more likely than current value
        //  or accept with probability α, by using a random number between 0 and 1.
        if (randgen.runif() < acceptanceProbability) {
            tracker.setIteration(proposal, i + 1);
            accepted++;
        }
        // reject
        else {
            tracker.setIteration(currentState, i + 1);
            rejected++;
        }
    }
    console.log("Acceptance ratio: " + accepted / (accepted + rejected));
    console.log("Bad proposal ratio: " + n_zeroes / (n_zeroes + rejected));
    return tracker;
};

var GMTMSampler = function (model, firstGuess, proposalSigma, observations, nTrials, nIter, wFunc) {
    /**
     * Sample parameters from the posterior distribution using Generalized Multiple Try Metropolis
     *
     * http://machinelearning.wustl.edu/mlpapers/paper_files/AISTATS2010_PandolfiBF10.pdf
     *
     * Arguments:
     * `model`, object containing the posterior, likelihood and prior functions.
     * `firstGuess`, object containing initial guess for each parameter, with parameter names as keys.
     * `proposalSigma`, object containing corresponding proposal standard deviation for each parameter, with parameter names as keys.
     * `observations`, array of individual observation values
     * `nIter`, integer number of iterations to run sampler.
     * `nTrials`, integer number of trial proposals to draw for multiple-try method.
     * `wFunc`, weights function of the MTM algorithm. Options: MTM, MTM-I, or MTM-inv.
     * Returns:
     * `tracker`, updated tracker object containing samples
     */

        // Set initial entry to be first guess and calculate the initial posterior density
    firstGuess = add_data(firstGuess, model.logPosterior(firstGuess, observations));
    var tracker = new Tracker(Object.keys(firstGuess), nIter);
    tracker.setIteration(firstGuess, 0);

    var currentState;
    var trial, trialWeights, trialSet;
    var draw, proposal, proposalProbability;
    var realization, realizWeights, realizationSet, realizProbability;
    var wStar, acceptanceProbability;

    // Decide which function to use as the weights (wstar) function
    if (wFunc === "MTM-inv") {
        wStar = wStarMTMinv;
    } else if (wFunc === "MTM-I") {
        wStar = wStarMTMI;
    } else if (wFunc === "MTM") {
        wStar = wStarMTM;
    } else {
        throw "Invalid w* function selected!";
    }

    var accepted = 0;
    var rejected = 0;
    // main sampling loop
    for (var i = 0; i < nIter; i++) {
        // Make new proposals
        currentState = tracker.getIteration(i); // Contains parameters and posterior density
        trialSet = new Array(nTrials);
        trialWeights = new Array(nTrials);

        // Draw trials y_1 through y_k
        for (var p = 0; p < nTrials; p++) {
            trialSet[p] = new Array(2);
            trial = propose(model, currentState, proposalSigma);
            trial = add_data(trial, model.logPosterior(trial, observations));
            trialSet[p] = trial;
        }
        for (var w = 0; w < nTrials; w++) {
            trialWeights[w] = wStar(model, trialSet[w], currentState, proposalSigma);
        }
        draw = drawWeightedSample(trialWeights); // [chosen index, probability]
        proposal = trialSet[draw[0]]; // Contains parameters and posterior density

        // p_y
        proposalProbability = draw[1]; // contains the probability of it being selected out of all trials

        realizationSet = new Array(nTrials);
        realizWeights = new Array(nTrials);

        //Draw Realizations x*_1 through X*_(k-1), set x*_k = x_t
        for (var q = 0; q < nTrials; q++) {
            realizationSet[q] = new Array(2);
            if (q === nTrials - 1) {
                // probability of xt > y
                realizationSet[q] = currentState;
            } else {
                // y > xk
                realization = propose(model, proposal, proposalSigma);
                realization = add_data(realization, model.logPosterior(realization, observations));
                realizationSet[q] = realization;
            }
        }
        for (var v = 0; v < nTrials; v++) {
            realizWeights[v] = wStar(model, realizationSet[v], proposal, proposalSigma);
        }
        // Calculate p_x_t
        realizProbability = realizWeights[nTrials - 1] / (realizWeights.reduce(function (total, num) {
                return total + num
            }));
        // The transition from xt to xt+1 = y is  accepted with probability α, min(1, π(y)T(y,x_t)p_xt/π(x_t)T(x_t,y)p_y

        var numerator = proposal.logposterior + proposalDensity(model, proposal, currentState, proposalSigma) + Math.log(realizProbability);
        var denominator = currentState.logposterior + proposalDensity(model, currentState, proposal, proposalSigma) + Math.log(proposalProbability);
        acceptanceProbability = Math.min(1, Math.exp(numerator) / Math.exp(denominator));
        //  If α ≥ 1, then proposal is more likely than current value
        //  or accept with probability α, by using a random number between 0 and 1.
        if (randgen.runif() < acceptanceProbability) {
            tracker.setIteration(proposal, i + 1);
            accepted++;
        }
        // reject
        else {
            tracker.setIteration(currentState, i + 1);
            rejected++;
        }
    }
    console.log("Acceptance ratio:" + accepted / (accepted + rejected));
    return tracker;

};

var DelayedRejectionSampler = function (model, firstGuess, proposalSigma, observations, nRejects, nIter) {
    /** WORK IN PROGRESS
     * Sample parameters from the posterior distribution
     *
     * Arguments:
     * `model`, object containing the posterior, likelihood and prior functions.
     * `firstGuess`, object containing initial guess for each parameter, with parameter names as keys.
     * `proposalSigma`, object containing corresponding proposal standard deviation for each parameter, with parameter names as keys.
     * `observations`, array of individual observation values
     * `nRejects`, integer max number of rejections (currently ignored and set to 2).
     * `nIter`, integer number of iterations to run sampler.
     *
     * Returns:
     * `chain`, array of samples as paired object with parameter values, with parameternames as keys,
     *  and corresponding log posterior probability, for each sample.
     */
        // Set initial entry to be first guess and calculate the initial posterior density
    firstGuess = add_data(firstGuess, model.logPosterior(firstGuess, observations));
    var tracker = new Tracker(Object.keys(firstGuess), nIter);
    tracker.setIteration(firstGuess, 0);

    var currentState;
    var proposal, proposalProb;
    var backwardProb;
    var acceptanceProbability;
    var accepted = 0;
    var rejected = 0;

    // main sampling loop
    for (var i = 0; i <= nIter; i++) {
        // Make a new proposal
        currentState = tracker.getIteration(i);
        proposal = propose(model, currentState, proposalSigma);
        try {
            proposal = add_data(proposal, model.logPosterior(proposal, observations));
        } catch(err) {
        	console.log("Error");
        	if (err == "Zero probability") {
        		console.log(err);
        		console.log("R-tard");
        		tracker.setIteration(currentState, i + 1);
        		rejected++;
        		continue;
        	}
        }
        proposalProb = proposalDensity(model, proposal, currentState, proposalSigma, true);
        backwardProb = proposalDensity(model, currentState, proposal, proposalSigma, true);

        var numerator = Math.exp(proposal.logposterior + backwardProb);
        var denominator = Math.exp(currentState.logposterior + proposalProb);

        acceptanceProbability = numerator / denominator;

        //  If α ≥ 1, then proposal is more likely than current value
        //  or accept with probability α, by using a random number between 0 and 1.
        if (randgen.runif() < acceptanceProbability) {
            tracker.setIteration(proposal, i + 1);
            accepted++;
        }
        // reject
        else {
            tracker.setIteration(currentState, i + 1);
            rejected++;
        }
    }
    console.log("Acceptance ratio:" + accepted / (accepted + rejected));
    return tracker;
};
